FROM cloudron/base:1.0.0@sha256:147a648a068a2e746644746bbfb42eb7a50d682437cead3c67c933c546357617

ARG VERSION=10.4.3-1
ARG JELLYFIN_LDAP_VERSION="v6/jellyfin-plugin-ldapauth_6.0"

RUN mkdir -p /app/code
WORKDIR /app/code

RUN apt-get install -y apt-transport-https unzip
RUN add-apt-repository universe
RUN wget -O - https://repo.jellyfin.org/ubuntu/jellyfin_team.gpg.key | sudo apt-key add -
RUN echo "deb [arch=$( dpkg --print-architecture )] https://repo.jellyfin.org/ubuntu $( lsb_release -c -s ) main" | tee /etc/apt/sources.list.d/jellyfin.list
RUN apt-get update -y
RUN apt-get install -y jellyfin=${VERSION}
RUN mkdir -p /app/code/jellyfin
RUN wget "https://github.com/jellyfin/jellyfin-plugin-ldapauth/releases/download/${JELLYFIN_LDAP_VERSION}.zip" -O /app/code/jellyfin/jellyfin_ldap.zip
RUN unzip /app/code/jellyfin/jellyfin_ldap.zip -d /app/code/jellyfin/jellyfin_ldap
RUN rm /app/code/jellyfin/jellyfin_ldap.zip

COPY start.sh /app/code/

CMD [ "/app/code/start.sh" ]
