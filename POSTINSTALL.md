Jellyfin does not have a built-in way to upload content. This has to be done through the SFTP support in Cloudron.

After creating an admin account, media libraries have to be set up.

All libraries must point to a subfolder within `/app/data/libraries`.

For this purpose, the package already has subfolders created for common types:
* `/app/data/libraries/movies`
* `/app/data/libraries/shows`
* `/app/data/libraries/photos`
* `/app/data/libraries/music`

Those can be changed later.
