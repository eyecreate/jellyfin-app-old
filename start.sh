#!/bin/bash

set -eu

echo "=> Ensure directories"
if [ ! -d /app/data/jellyfin ]; then
  mkdir -p /app/data/jellyfin/data
  mkdir -p /app/data/jellyfin/config
  mkdir -p /app/data/jellyfin/log

  mkdir -p /app/data/jellyfin/data/plugins/LDAP\ Authentication
  cp /app/code/jellyfin/jellyfin_ldap/* /app/data/jellyfin/data/plugins/LDAP\ Authentication

  mkdir -p /app/data/libraries/movies/ /app/data/libraries/shows/ /app/data/libraries/photos/ /app/data/libraries/music/
fi
mkdir -p /tmp/jellyfin

echo "=> Write LDAP Config"
mkdir -p /app/data/jellyfin/data/plugins/configurations/
cat <<EOF > /app/data/jellyfin/data/plugins/configurations/LDAP-Auth.xml
<?xml version="1.0"?>
<PluginConfiguration xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <LdapServer>${LDAP_SERVER}</LdapServer>
    <LdapBaseDn>${LDAP_USERS_BASE_DN}</LdapBaseDn>
    <LdapPort>${LDAP_PORT}</LdapPort>
    <LdapSearchAttributes>username, mail</LdapSearchAttributes>
    <LdapUsernameAttribute>username</LdapUsernameAttribute>
    <LdapSearchFilter>(objectclass=user)</LdapSearchFilter>
    <LdapAdminFilter>(memberof=cn=admins,${LDAP_GROUPS_BASE_DN})</LdapAdminFilter>
    <LdapBindUser>${LDAP_BIND_DN}</LdapBindUser>
    <LdapBindPassword>${LDAP_BIND_PASSWORD}</LdapBindPassword>
    <CreateUsersFromLdap>true</CreateUsersFromLdap>
    <UseSsl>false</UseSsl>
</PluginConfiguration>
EOF


echo "=> Ensure permissions"
chown -R cloudron:cloudron /app/data /tmp /run

echo "=> Start jellyfin"
exec /usr/local/bin/gosu cloudron:cloudron /usr/lib/jellyfin/bin/jellyfin --datadir /app/data/jellyfin/data --configdir /app/data/jellyfin/config --logdir /app/data/jellyfin/log --cachedir /tmp/jellyfin
